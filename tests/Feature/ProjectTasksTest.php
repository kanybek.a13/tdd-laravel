<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Setup\ProjectFactory;
use Tests\TestCase;

class ProjectTasksTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_project_can_have_tasks()
    {
//        $this->signIn();
//
//        $project = \App\Models\Project::factory()->create(['owner_id' => auth()->id()]);

//        $project = auth()->user()->projects()->create(
//            \App\Models\Project::factory()->raw()
//        );
        $project = ProjectFactory::create();

        $this->actingAs($project->owner)
            ->post($project->path() . '/tasks', ['body' => "Task body"]);

        $this->get($project->path())
            ->assertSee('Task body');
    }

    /** @test */
    public function task_requires_a_body()
    {
//        $this->signIn();
//
//        $project = auth()->user()->projects()->create(
//            \App\Models\Project::factory()->raw()
//        );

        $project = ProjectFactory::create();

        $attributes = \App\Models\Project::factory()->raw(['description' => '']);

        $this->actingAs($project->owner)
            ->post('/projects', $attributes)
            ->assertSessionHasErrors('description');
    }

    /** @test */
    public function a_task_can_be_updated()
    {
//        $this->withoutExceptionHandling();

//        $project = auth()->user()->projects()->create(
//            \App\Models\Project::factory()->raw()
//        );
//
//        $task = $project->addTask("Test task");

//        $project = app(ProjectFactory::class)
//            ->ownedBy($this->signIn())
//            ->withTasks(1)
//            ->create();

        $project = ProjectFactory::withTasks(1)->create();

//        $this->patch($project->tasks->first()->path(), [
        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), [
            'body' => 'changed',
        ]);

        $this->assertDatabaseHas('tasks', [
            'body'=>'changed',
        ]);
    }

    /** @test */
    public function a_task_can_be_completed()
    {
        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), [
                'body' => 'changed',
                'completed' => true
            ]);

        $this->assertDatabaseHas('tasks', [
            'body' => 'changed',
            'completed'=> true
        ]);
    }

    /** @test */
    public function a_task_can_be_marked_as_incompleted()
    {
        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), [
                'body' => 'changed',
                'completed' => true
            ]);

        $this->patch($project->tasks->first()->path(), [
                'body' => 'changed',
                'completed' => false
            ]);

        $this->assertDatabaseHas('tasks', [
            'body' => 'changed',
            'completed'=> false
        ]);
    }

    /** @test */
    public function only_the_owner_of_a_project_may_add_tasks()
    {
        $this->signIn();

        $project = \App\Models\Project::factory()->create();

        $this->post($project->path() . '/tasks', ['body' => "Task body"])
            ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', ['body'=>"Task body"]);
    }

    /** @test */
    public function only_the_owner_of_a_project_may_update_tasks()
    {
        $this->signIn();

        $project = ProjectFactory::withTasks(1)->create();

        $this->patch($project->tasks[0]->path(), ['body' => "changed"])
            ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', ['body'=>"changed"]);
    }
}
