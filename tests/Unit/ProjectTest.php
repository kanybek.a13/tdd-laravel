<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProjectTest extends TestCase
{
//    use RefreshDatabase;

    /** @test */
    public function it_has_a_path()
    {
        $project = \App\Models\Project::factory()->create();

        $this->assertEquals('/projects/' . $project->id, $project->path());
    }

    /** @test */
    public function it_belongsto_an_owner()
    {
        $project = \App\Models\Project::factory()->create();

        $this->assertInstanceOf('App\Models\User', $project->owner);
    }

    /** @test */
    public function it_can_add_task()
    {
        $project = \App\Models\Project::factory()->create();

        $task = $project->addTask('test task');

        $this->assertCount(1, $project->tasks);
        $this->assertTrue($project->tasks->contains($task));
    }

    /** @test */
    public function it_can_invite_a_user()
    {
        $project = \App\Models\Project::factory()->create();

        $project->invite($newUser = \App\Models\User::factory()->create());

        $this->assertTrue($project->members->contains($newUser));
    }
}
