<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Facades\Tests\Setup\ProjectFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use App\Models\Project;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_has_projects()
    {
        $user =  User::factory()->create();

        $this->assertInstanceOf(Collection::class, $user->projects);
    }

    /** @test */
    public function a_user_has_all_projects()
    {
        $user = $this->signIn();

        $project = ProjectFactory::ownedBy($user)->create();

        $this->assertCount(1, $user->allProjects());

        $user2 = User::factory()->create();
        $user3 = User::factory()->create();

        $user2_projects = ProjectFactory::ownedBy($user2)->create();
        $user2_projects->invite($user3);

//        ProjectFactory::ownedBy($user2)->create()->invite($user);

        $this->assertCount(1, $user->allProjects());
//        $this->assertCount(2, $user->allProjects());

        $user2_projects->invite($user);
        $this->assertCount(2, $user->allProjects());
    }
}
